using aRICE.Classes;
using Avalonia;
using Avalonia.Controls;
using Avalonia.Input;
using Avalonia.Interactivity;
using Avalonia.Markup.Xaml;

namespace aRICE
{
    public class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
#if DEBUG
            this.AttachDevTools();
#endif
        }

        private void InitializeComponent()
        {
            Resources.Add("LaTeX", "\\text{Enter a new equation}");
            AvaloniaXamlLoader.Load(this);
        }

        readonly Reaction reaction = new Reaction();

        void CommonClick(object sender, RoutedEventArgs e)
        {
            bool terminalClear = false;
            Button button = (Button)e.Source!;
            TextBox textBox;
            switch (button.Name)
            {
                case "buttonSpeciesOk":
                    textBox = this.FindControl<TextBox>("textBoxSpecies");
                    SpeciesOk(textBox);
                    break;

                case "buttonSpeciesDelete":
                    textBox = this.FindControl<TextBox>("textBoxSpecies");
                    textBox.Text = reaction.Delete_name();
                    break;
                case "buttonCoefOk":
                    textBox = this.FindControl<TextBox>("textBoxCoef");
                    CoefOk(textBox);
                    break;
                case "buttonCoefDelete":
                    textBox = this.FindControl<TextBox>("textBoxCoef");
                    textBox.Text = reaction.Delete_coef();
                    break;
                case "buttonAmountOk":
                    textBox = this.FindControl<TextBox>("textBoxAmount");
                    AmountOk(textBox);
                    break;
                case "buttonAmountDelete":
                    textBox = this.FindControl<TextBox>("textBoxAmount");
                    textBox.Text = reaction.Delete_quantity();
                    break;
                case "buttonStartReaction":
                    reaction.Result = string.Empty;
                    reaction.XmaxFinder();
                    reaction.Computation();
                    reaction.Stoichiometric();
                    break;
                case "buttonCheckReaction":
                    reaction.Check_process();
                    break;
                case "buttonResetAll":
                    textBox = this.FindControl<TextBox>("textBoxSpecies");
                    textBox.Text = reaction.Delete_name();
                    textBox = this.FindControl<TextBox>("textBoxCoef");
                    textBox.Text = reaction.Delete_coef();
                    textBox = this.FindControl<TextBox>("textBoxAmount");
                    textBox.Text = reaction.Delete_quantity();
                    terminalClear = true;
                    break;
                case "buttonBoxTerminal":
                    terminalClear = true;
                    break;
            }
            Terminal(terminalClear);
            e.Handled = true;
        }

        void CommonEnter(object sender, KeyEventArgs k)
        {
            if (k.Key == Key.Enter)
            {
                TextBox textBox = (TextBox)k.Source!;
                switch (textBox?.Name)
                {
                    case "textBoxSpecies":
                        SpeciesOk(textBox!);
                        break;
                    case "textBoxCoef":
                        CoefOk(textBox!);
                        break;
                    case "textBoxAmount":
                        AmountOk(textBox!);
                        break;
                }
                Terminal(false);
            }

            k.Handled = true;
        }

        void SpeciesOk(TextBox textBox)
        {
            textBox.Text = textBox.Text != null
                ? reaction.Add_name(textBox.Text)
                : reaction.Add_name(string.Empty);
        }

        void CoefOk(TextBox textBox)
        {
            textBox.Text = textBox.Text != null
                ? reaction.Add_coef(textBox.Text)
                : reaction.Add_coef(string.Empty);
        }

        void AmountOk(TextBox textBox)
        {
            textBox.Text = textBox.Text != null
                ? reaction.Add_quantity(textBox.Text.Replace(".", ","))
                : reaction.Add_coef(string.Empty);
        }

        void Terminal(bool terminalClear)
        {
            TextBox textBox = this.FindControl<TextBox>("textBoxTerminal");
            textBox.Text = terminalClear ? string.Empty : reaction.Refresh() + reaction.Result;

            Render render = new Render();
            Resources.Clear();
            Resources.Add("LaTeX", render.Latex(reaction.nameInput, reaction.coefInput, reaction.nameOutput, reaction.coefOutput));
        }

    }
}
