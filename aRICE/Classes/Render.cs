﻿using Avalonia.Controls;
using System;
using System.Collections.Generic;

namespace aRICE.Classes
{
    class Render : UserControl
    {
        public string Latex(List<string> nameInputList, List<decimal> coefInputList, List<string> nameOutputList,
            List<decimal> coefOutputList)
        {
            int i;
            string formula = string.Empty;
            for (i = 0; i < nameInputList.Count; i++)
            {
                try
                {
                    formula += coefInputList[i] != 1 ? coefInputList[i] + " " + nameInputList[i] : nameInputList[i];
                }
                catch (Exception)
                {
                    formula += "? " + nameInputList[i];
                }

                if (i < nameInputList.Count - 1) formula += " + ";
            }


            formula += nameOutputList.Count > 0 ? " \\longrightarrow " : string.Empty;

            for (i = 0; i < nameOutputList.Count; i++)
            {
                try
                {
                    formula += coefOutputList[i] != 1
                        ? coefOutputList[i] + " " + nameOutputList[i]
                        : nameOutputList[i];
                }
                catch (Exception)
                {
                    formula += " ? " + nameOutputList[i];
                }

                if (i < nameOutputList.Count - 1) formula += " + ";
            }

            return formula;
        }
    }
}
