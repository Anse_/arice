﻿using Avalonia.Controls;
using MessageBox.Avalonia.BaseWindows.Base;
using MessageBox.Avalonia.Enums;
using System;
using System.Collections.Generic;
using System.Linq;

namespace aRICE.Classes
{
    class Reaction : ContentControl
    {

        public List<string> nameInput = new List<string> { };
        public List<decimal> coefInput = new List<decimal> { };
        public List<object> nInput = new List<object> { };

        public List<string> nameOutput = new List<string> { };
        public List<decimal> coefOutput = new List<decimal> { };
        public List<object> nOutput = new List<object> { };

        readonly List<decimal> xf = new List<decimal> { };

        public List<decimal> reagentComp = new List<decimal> { };
        public List<decimal> productComp = new List<decimal> { };

        private int i;
        public string Result = string.Empty;

        public string Add_name(string x)
        {
            if (x.EndsWith("/") & (x.Length > 1)) nameOutput.Add(x.Remove(x.Length - 1));
            else if ((x.Length > 0) & (!x.EndsWith("/"))) nameInput.Add(x);
            else
            {
                IMsBoxWindow<ButtonResult> messageBoxErrorName =
                    MessageBox.Avalonia.MessageBoxManager.GetMessageBoxStandardWindow("Error",
                        "A name cannot be empty!");
                messageBoxErrorName.Show();
            }

            return string.Empty;
        }

        public string Delete_name()
        {
            nameInput.Clear();
            nameOutput.Clear();
            return string.Empty;
        }

        public string Add_coef(string x)
        {
            if (x.EndsWith("/") & (x.Length > 1) &&
                ((decimal.TryParse(x.Remove(x.Length - 1), out decimal coefOutResult)))) coefOutput.Add(coefOutResult);
            else if ((x.Length > 0) && (decimal.TryParse(x, out decimal coefInResult))) coefInput.Add(coefInResult);
            else
            {
                IMsBoxWindow<ButtonResult> messageBoxErrorDecimal =
                    MessageBox.Avalonia.MessageBoxManager.GetMessageBoxStandardWindow("Error",
                        "The coefficient must be a decimal!");
                messageBoxErrorDecimal.Show();
            }

            return string.Empty;
        }

        public string Delete_coef()
        {
            coefInput.Clear();
            coefOutput.Clear();
            return string.Empty;
        }

        public string Add_quantity(string x)
        {

            if (x == "*/") nOutput.Add("*");
            else if(((x.Length > 1) & (x.EndsWith("/"))) &&
                (decimal.TryParse(x.Remove(x.Length - 1), out decimal nOutResult))) nOutput.Add(nOutResult);
            else if (x == "*") nInput.Add(x);
            else if ((x.Length > 0) & (decimal.TryParse(x, out decimal nInResult))) nInput.Add(nInResult);
            else
            {
                IMsBoxWindow<ButtonResult> messageBoxErrorDecimal =
                    MessageBox.Avalonia.MessageBoxManager.GetMessageBoxStandardWindow("Error",
                        "The amount must be a decimal!");
                messageBoxErrorDecimal.Show();
            }

            return string.Empty;
        }

        public string Delete_quantity()
        {
            nInput.Clear();
            nOutput.Clear();
            return string.Empty;
        }

        public string Delete_all()
        {
            nameInput.Clear();
            coefInput.Clear();
            nInput.Clear();
            nameOutput.Clear();
            coefOutput.Clear();
            nOutput.Clear();
            reagentComp.Clear();
            productComp.Clear();

            return string.Empty;
        }

        public void Check_process()
        {
            if (nameInput.Count != coefInput.Count ^ nameInput.Count != nInput.Count ^
                nameOutput.Count != coefOutput.Count ^ nameOutput.Count != nOutput.Count)
            {
                IMsBoxWindow<ButtonResult> messageBoxErrorEquality =
                    MessageBox.Avalonia.MessageBoxManager.GetMessageBoxStandardWindow("Error",
                        "Process cannot start : every entry doesn't have the same amount, please check if you didn't forget one somewhere!");
                messageBoxErrorEquality.Show();
            }
            else if (nameInput.Count < 2)
            {
                IMsBoxWindow<ButtonResult> messageBoxErrorMin =
                    MessageBox.Avalonia.MessageBoxManager.GetMessageBoxStandardWindow("Error",
                        "Process cannot start : You need at least 2 reagents !");
                messageBoxErrorMin.Show();
            }
        }

        public string Refresh()
        {
            string output = "-- Setup --" + Environment.NewLine + "Name of reagents:" + Environment.NewLine;
            for (i = 0; i < nameInput.Count; i++)
            {
                output += nameInput[i];
                output += i < nameInput.Count - 1 ? ";" : ".";
            }

            output += Environment.NewLine + "Coefficients:" + Environment.NewLine;
            for (i = 0; i < coefInput.Count; i++)
            {
                output += coefInput[i];
                output += i < coefInput.Count - 1 ? ";" : ".";
            }

            output += Environment.NewLine + "Amount of reagents:" + Environment.NewLine;
            for (i = 0; i < nInput.Count; i++)
            {
                output += nInput[i];
                output += i < nInput.Count - 1 ? "mol;" : "mol.";
            }

            output += Environment.NewLine + "Name of product(s):" + Environment.NewLine;
            for (i = 0; i < nameOutput.Count; i++)
            {
                output += nameOutput[i];
                output += i < nameOutput.Count - 1 ? ";" : ".";
            }

            output += Environment.NewLine + "Coefficient(s)" + Environment.NewLine;
            for (i = 0; i < coefOutput.Count; i++)
            {
                output += coefOutput[i];
                output += i < coefOutput.Count - 1 ? ";" : ".";
            }

            output += Environment.NewLine + "Amount of product(s):" + Environment.NewLine;
            for (i = 0; i < nOutput.Count; i++)
            {
                output += nOutput[i];
                output += i < nOutput.Count - 1 ? " mol;" : " mol." + Environment.NewLine;
            }

            return output;
        }

        public void XmaxFinder()
        {
            xf.Clear();

            Result += Environment.NewLine + "-- xmax finder --" + Environment.NewLine;

            for (i = 0; i < nameInput.Count; i++)
            {
                if (nInput[i] is string) Result += nameInput[i] + " is surplus." + Environment.NewLine;
                else
                {
                    xf.Add((decimal)nInput[i] / coefInput[i]);
                    Result += "xf for " + nameInput[i] + ": " + xf.Last() + Environment.NewLine;
                }
            }
            Result += "xmax = " + xf.Min() + Environment.NewLine;
        }

        public void Computation()
        {
            Result += Environment.NewLine + "-- Computation --" + Environment.NewLine;
            for (i = 0; i < nameInput.Count; i++)
            {
                if (nInput[i] is string) Result += nameInput[i] + " is surplus";
                else
                {
                    reagentComp.Add((decimal)nInput[i] - xf.Min() * coefInput[i]);
                    Result += nameInput[i] + ": " + reagentComp[i] + "mol." + Environment.NewLine;
                }
            }

            for (i = 0; i < nameOutput.Count; i++)
            {
                if (nOutput[i] is string) Result += nameOutput[i] + " is surplus";
                else
                {
                    productComp.Add((decimal)nOutput[i] + xf.Min() * coefOutput[i]);
                    Result += nameOutput[i] + ": " + productComp[i] + "mol." + Environment.NewLine;
                }
            }
        }

        public void Stoichiometric()
        {
            decimal sum = reagentComp.Sum();
            Result += Environment.NewLine + "-- Is this reaction stoichiometric? --" + Environment.NewLine + "Σ = " +
                      sum + Environment.NewLine;
            Result += sum == 0
                ? "This reaction is stoichiometric."
                : "This reaction is not stoichiometric.";
        }
    }
}
